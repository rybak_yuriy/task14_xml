package com.rybak.parser;

import com.rybak.parser.dom.DomRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class XMLTransformer {

    final File xml = new File("src/main/resources/candies.xml");
    final File xsl = new File("src/main/resources/candies.xsl");

    private final String outputHTMLFileName = "candies.html";
    private final String outputXMLFileName = "candy1.xml";

    public void transformInfoHTMLAndBack() {
        TransformerFactory tFactory = TransformerFactory.newInstance();
        try {
            transformIntoHTML(tFactory);
            transformIntoXML(tFactory);
        } catch (FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private void transformIntoHTML(TransformerFactory tFactory)
            throws FileNotFoundException, TransformerException {
        Source xslDoc = new StreamSource(xsl);
        Source xmlDoc = new StreamSource(xml);
        OutputStream htmlFile;
        htmlFile = new FileOutputStream(outputHTMLFileName);
        Transformer transform = tFactory.newTransformer(xslDoc);
        transform.transform(xmlDoc, new StreamResult(htmlFile));
    }

    private void transformIntoXML(TransformerFactory tFactory)
            throws TransformerException {
        DomRunner domRunner = new DomRunner();
        Document doc = domRunner.buildDoc(xml);
        Node root = doc.getElementsByTagName("candies").item(0);
        root.setTextContent("Changed root element");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(outputXMLFileName));
        Transformer transformer = tFactory.newTransformer();
        transformer.transform(source, result);
    }

}
