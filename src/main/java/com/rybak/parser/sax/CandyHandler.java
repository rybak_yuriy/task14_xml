package com.rybak.parser.sax;

import com.rybak.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class CandyHandler extends DefaultHandler {

    private List<Candy> candies = new ArrayList<>();
    private Candy candy = null;
    private Ingredient ingredient = null;
    private Value value = null;

    private boolean NAME = false;
    private boolean ENERGY = false;
    private boolean TYPE = false;
    private boolean WATER = false;
    private boolean SUGAR = false;
    private boolean FRUCTOSE = false;
    private boolean VANILLIN = false;
    private boolean PROTEINS = false;
    private boolean FATS = false;
    private boolean CARBOHYDRATE = false;
    private boolean PRODUCTION = false;


    public List<Candy> getCandiesList() {
        return candies;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candy = new Candy();
        } else if (qName.equalsIgnoreCase("name")) {
            NAME = true;
        } else if (qName.equalsIgnoreCase("energy")) {
            ENERGY = true;
        } else if (qName.equalsIgnoreCase("type")) {
            TYPE = true;
        } else if (qName.equalsIgnoreCase("ingredients")) {
            ingredient = new Ingredient();
        } else if (qName.equalsIgnoreCase("water")) {
            WATER = true;
        } else if (qName.equalsIgnoreCase("sugar")) {
            SUGAR = true;
        } else if (qName.equalsIgnoreCase("fructose")) {
            FRUCTOSE = true;
        } else if (qName.equalsIgnoreCase("vanillin")) {
            VANILLIN = true;
        } else if (qName.equalsIgnoreCase("value")) {
            value = new Value();
        } else if (qName.equalsIgnoreCase("proteins")) {
            PROTEINS = true;
        } else if (qName.equalsIgnoreCase("fats")) {
            FATS = true;
        } else if (qName.equalsIgnoreCase("carbohydrates")) {
            CARBOHYDRATE = true;
        } else if (qName.equalsIgnoreCase("production")) {
            PRODUCTION = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candies.add(candy);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (NAME) {
            candy.setName(new String(ch, start, length));
            NAME = false;
        } else if (ENERGY) {
            candy.setEnergy(Integer.parseInt(new String(ch, start, length)));
            ENERGY = false;
        } else if (TYPE) {
            candy.setType(Type.valueOf(new String(ch, start, length).toUpperCase()));
            TYPE = false;
        } else if (WATER) {
            ingredient.setWater(Integer.parseInt(new String(ch, start, length)));
            WATER = false;
        } else if (SUGAR) {
            ingredient.setSugar(Integer.parseInt(new String(ch, start, length)));
            SUGAR = false;
        } else if (FRUCTOSE) {
            ingredient.setFructose(Integer.parseInt(new String(ch, start, length)));
            FRUCTOSE = false;
        } else if (VANILLIN) {
            ingredient.setVanillin(Integer.parseInt(new String(ch, start, length)));
            VANILLIN = false;
        } else if (PROTEINS) {
            value.setProteins(Integer.parseInt(new String(ch, start, length)));
            PROTEINS = false;
        } else if (FATS) {
            value.setFats(Integer.parseInt(new String(ch, start, length)));
            FATS = false;
        } else if (CARBOHYDRATE) {
            value.setCarbohydrates(Integer.parseInt(new String(ch, start, length)));
            CARBOHYDRATE = false;
        } else if (PRODUCTION) {
            candy.setProduction(new String(ch, start, length));
            PRODUCTION = false;
            candy.setIngredient(ingredient);
            candy.setValue(value);
        }
    }

}
