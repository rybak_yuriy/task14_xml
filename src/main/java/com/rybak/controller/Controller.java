package com.rybak.controller;

import com.rybak.parser.XMLTransformer;
import com.rybak.parser.dom.DomRunner;
import com.rybak.parser.sax.SaxRunner;
import com.rybak.parser.stax.StAXRunner;


public class Controller {

    DomRunner domRunner;
    SaxRunner saxRunner;
    StAXRunner stAXRunner;
    XMLTransformer xmlTransformer;

    public Controller() {
        domRunner = new DomRunner();
        saxRunner = new SaxRunner();
        stAXRunner = new StAXRunner();
        xmlTransformer = new XMLTransformer();
    }

    public void parseByDomParser() {
        domRunner.run();
    }

    public void parseBySAXParser() {
        saxRunner.run();
    }

    public void parseByStAXParser() {
        stAXRunner.run();
    }

    public void transformIntoHTMLAndBack() {
        xmlTransformer.transformInfoHTMLAndBack();
    }
}
