package com.rybak.model;

public enum Type {

    CHOCOLATE,
    CARAMELS,
    IRIS,
    LOLLIPOP

}
